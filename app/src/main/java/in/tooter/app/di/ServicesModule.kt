/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package `in`.tooter.app.di

import android.content.Context
import `in`.tooter.app.service.SendTootService
import `in`.tooter.app.service.ServiceClient
import `in`.tooter.app.service.ServiceClientImpl
import dagger.Module
import dagger.Provides
import dagger.android.ContributesAndroidInjector

@Module
abstract class ServicesModule {
    @ContributesAndroidInjector
    abstract fun contributesSendTootService(): SendTootService

    @Module
    companion object {
        @Provides
        @JvmStatic
        fun providesServiceClient(context: Context): ServiceClient {
            return ServiceClientImpl(context)
        }
    }
}