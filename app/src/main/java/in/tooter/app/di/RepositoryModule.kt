/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package `in`.tooter.app.di

import com.google.gson.Gson
import `in`.tooter.app.db.AccountManager
import `in`.tooter.app.db.AppDatabase
import `in`.tooter.app.network.MastodonApi
import `in`.tooter.app.repository.TimelineRepository
import `in`.tooter.app.repository.TimelineRepositoryImpl
import dagger.Module
import dagger.Provides

@Module
class RepositoryModule {
    @Provides
    fun providesTimelineRepository(
            db: AppDatabase,
            mastodonApi: MastodonApi,
            accountManager: AccountManager,
            gson: Gson
    ): TimelineRepository {
        return TimelineRepositoryImpl(db.timelineDao(), mastodonApi, accountManager, gson)
    }
}