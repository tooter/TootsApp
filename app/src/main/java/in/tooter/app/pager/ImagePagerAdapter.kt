/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package `in`.tooter.app.pager

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import `in`.tooter.app.ViewMediaAdapter
import `in`.tooter.app.entity.Attachment
import `in`.tooter.app.fragment.ViewMediaFragment
import java.lang.ref.WeakReference

class ImagePagerAdapter(
        activity: FragmentActivity,
        private val attachments: List<Attachment>,
        private val initialPosition: Int
) : ViewMediaAdapter(activity) {

    private var didTransition = false
    private val fragments = MutableList<WeakReference<ViewMediaFragment>?>(attachments.size) { null }

    override fun getItemCount() = attachments.size

    override fun createFragment(position: Int): Fragment {
        if (position >= 0 && position < attachments.size) {
            // Fragment should not wait for or start transition if it already happened but we
            // instantiate the same fragment again, e.g. open the first photo, scroll to the
            // forth photo and then back to the first. The first fragment will try to start the
            // transition and wait until it's over and it will never take place.
            val fragment = ViewMediaFragment.newInstance(
                    attachment = attachments[position],
                    shouldStartPostponedTransition = !didTransition && position == initialPosition
            )
            fragments[position] = WeakReference(fragment)
            return fragment
        } else {
            throw IllegalStateException()
        }
    }

   override fun onTransitionEnd(position: Int) {
        this.didTransition = true
        fragments[position]?.get()?.onTransitionEnd()
    }
}
