/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package `in`.tooter.app.components.report.fragments


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import `in`.tooter.app.R
import `in`.tooter.app.components.report.ReportViewModel
import `in`.tooter.app.components.report.Screen
import `in`.tooter.app.di.Injectable
import `in`.tooter.app.di.ViewModelFactory
import `in`.tooter.app.util.Loading
import `in`.tooter.app.util.hide
import `in`.tooter.app.util.show
import kotlinx.android.synthetic.main.fragment_report_done.*
import javax.inject.Inject


class ReportDoneFragment : Fragment(), Injectable {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private val viewModel: ReportViewModel by viewModels({ requireActivity() }) { viewModelFactory }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_report_done, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        textReported.text = getString(R.string.report_sent_success, viewModel.accountUserName)
        handleClicks()
        subscribeObservables()
    }

    private fun subscribeObservables() {
        viewModel.muteState.observe(viewLifecycleOwner, Observer {
            if (it !is Loading) {
                buttonMute.show()
                progressMute.show()
            } else {
                buttonMute.hide()
                progressMute.hide()
            }

            buttonMute.setText(when (it.data) {
                true -> R.string.action_unmute
                else -> R.string.action_mute
            })
        })

        viewModel.blockState.observe(viewLifecycleOwner, Observer {
            if (it !is Loading) {
                buttonBlock.show()
                progressBlock.show()
            }
            else{
                buttonBlock.hide()
                progressBlock.hide()
            }
            buttonBlock.setText(when (it.data) {
                true -> R.string.action_unblock
                else -> R.string.action_block
            })
        })

    }

    private fun handleClicks() {
        buttonDone.setOnClickListener {
            viewModel.navigateTo(Screen.Finish)
        }
        buttonBlock.setOnClickListener {
            viewModel.toggleBlock()
        }
        buttonMute.setOnClickListener {
            viewModel.toggleMute()
        }
    }

    companion object {
        fun newInstance() = ReportDoneFragment()
    }

}
