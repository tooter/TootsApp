/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package `in`.tooter.app.components.notifications

import android.content.Context
import android.util.Log
import androidx.work.ListenableWorker
import androidx.work.Worker
import androidx.work.WorkerFactory
import androidx.work.WorkerParameters
import `in`.tooter.app.db.AccountEntity
import `in`.tooter.app.db.AccountManager
import `in`.tooter.app.entity.Notification
import `in`.tooter.app.network.MastodonApi
import `in`.tooter.app.util.isLessThan
import java.io.IOException
import javax.inject.Inject

class NotificationWorker(
        private val context: Context,
        params: WorkerParameters,
        private val mastodonApi: MastodonApi,
        private val accountManager: AccountManager
) : Worker(context, params) {

    override fun doWork(): Result {
        val accountList = accountManager.getAllAccountsOrderedByActive()
        for (account in accountList) {
            if (account.notificationsEnabled) {
                try {
                    Log.d(TAG, "getting Notifications for " + account.fullName)
                    val notificationsResponse = mastodonApi.notificationsWithAuth(
                            String.format("Bearer %s", account.accessToken),
                            account.domain
                    ).execute()
                    val notifications = notificationsResponse.body()
                    if (notificationsResponse.isSuccessful && notifications != null) {
                        onNotificationsReceived(account, notifications)
                    } else {
                        Log.w(TAG, "error receiving notifications")
                    }
                } catch (e: IOException) {
                    Log.w(TAG, "error receiving notifications", e)
                }
            }
        }
        return Result.success()
    }

    private fun onNotificationsReceived(account: AccountEntity, notificationList: List<Notification>) {
        val newId = account.lastNotificationId
        var newestId = ""
        var isFirstOfBatch = true
        notificationList.reversed().forEach { notification ->
            val currentId = notification.id
            if (newestId.isLessThan(currentId)) {
                newestId = currentId
            }
            if (newId.isLessThan(currentId)) {
                NotificationHelper.make(context, notification, account, isFirstOfBatch)
                isFirstOfBatch = false
            }
        }
        account.lastNotificationId = newestId
        accountManager.saveAccount(account)
    }

    companion object {
        private const val TAG = "NotificationWorker"
    }

}

class NotificationWorkerFactory @Inject constructor(
        val api: MastodonApi,
        val accountManager: AccountManager
): WorkerFactory() {

    override fun createWorker(appContext: Context, workerClassName: String, workerParameters: WorkerParameters): ListenableWorker? {
        if(workerClassName == NotificationWorker::class.java.name) {
            return NotificationWorker(appContext, workerParameters, api, accountManager)
        }
        return null
    }
}
