/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package `in`.tooter.app.components.instancemute.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import `in`.tooter.app.R
import `in`.tooter.app.components.instancemute.interfaces.InstanceActionListener
import kotlinx.android.synthetic.main.item_muted_domain.view.*

class DomainMutesAdapter(private val actionListener: InstanceActionListener): RecyclerView.Adapter<DomainMutesAdapter.ViewHolder>() {
    var instances: MutableList<String> = mutableListOf()
    var bottomLoading: Boolean = false
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_muted_domain, parent, false), actionListener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setupWithInstance(instances[position])
    }

    override fun getItemCount(): Int {
        var count = instances.size
        if (bottomLoading)
            ++count
        return count
    }

    fun addItems(newInstances: List<String>) {
        val end = instances.size
        instances.addAll(newInstances)
        notifyItemRangeInserted(end, instances.size)
    }

    fun addItem(instance: String) {
        instances.add(instance)
        notifyItemInserted(instances.size)
    }

    fun removeItem(position: Int)
    {
        if (position >= 0 && position < instances.size) {
            instances.removeAt(position)
            notifyItemRemoved(position)
        }
    }


    class ViewHolder(rootView: View, private val actionListener: InstanceActionListener): RecyclerView.ViewHolder(rootView) {
        fun setupWithInstance(instance: String) {
            itemView.muted_domain.text = instance
            itemView.muted_domain_unmute.setOnClickListener {
                actionListener.mute(false, instance, adapterPosition)
            }
        }
    }
}