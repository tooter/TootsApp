/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package `in`.tooter.app.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import `in`.tooter.app.R
import `in`.tooter.app.entity.MastoList
import `in`.tooter.app.util.ThemeUtils
import kotlinx.android.synthetic.main.item_picker_list.view.*

class ListSelectionAdapter(context: Context) : ArrayAdapter<MastoList>(context, R.layout.item_autocomplete_hashtag) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

        val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val view = convertView
                ?: layoutInflater.inflate(R.layout.item_picker_list, parent, false)

        getItem(position)?.let { list ->
            val title = view.title
            title.text = list.title
            val icon = ThemeUtils.getTintedDrawable(context, R.drawable.ic_list, R.attr.iconColor)
            title.setCompoundDrawablesRelativeWithIntrinsicBounds(icon, null, null, null)
        }

        return view
    }
}
