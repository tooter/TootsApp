/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package in.tooter.app.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import in.tooter.app.R;
import in.tooter.app.interfaces.StatusActionListener;
import in.tooter.app.util.StatusDisplayOptions;
import in.tooter.app.viewdata.StatusViewData;

import java.util.ArrayList;
import java.util.List;

public class ThreadAdapter extends RecyclerView.Adapter {
    private static final int VIEW_TYPE_STATUS = 0;
    private static final int VIEW_TYPE_STATUS_DETAILED = 1;

    private List<StatusViewData.Concrete> statuses;
    private StatusDisplayOptions statusDisplayOptions;
    private StatusActionListener statusActionListener;
    private int detailedStatusPosition;

    public ThreadAdapter(StatusDisplayOptions statusDisplayOptions, StatusActionListener listener) {
        this.statusDisplayOptions = statusDisplayOptions;
        this.statusActionListener = listener;
        this.statuses = new ArrayList<>();
        detailedStatusPosition = RecyclerView.NO_POSITION;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            default:
            case VIEW_TYPE_STATUS: {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_status, parent, false);
                return new StatusViewHolder(view);
            }
            case VIEW_TYPE_STATUS_DETAILED: {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.item_status_detailed, parent, false);
                return new StatusDetailedViewHolder(view);
            }
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        StatusViewData.Concrete status = statuses.get(position);
        if (position == detailedStatusPosition) {
            StatusDetailedViewHolder holder = (StatusDetailedViewHolder) viewHolder;
            holder.setupWithStatus(status, statusActionListener, statusDisplayOptions);
        } else {
            StatusViewHolder holder = (StatusViewHolder) viewHolder;
            holder.setupWithStatus(status, statusActionListener, statusDisplayOptions);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == detailedStatusPosition) {
            return VIEW_TYPE_STATUS_DETAILED;
        } else {
            return VIEW_TYPE_STATUS;
        }
    }

    @Override
    public int getItemCount() {
        return statuses.size();
    }

    public void setStatuses(List<StatusViewData.Concrete> statuses) {
        this.statuses.clear();
        this.statuses.addAll(statuses);
        notifyDataSetChanged();
    }

    public void addItem(int position, StatusViewData.Concrete statusViewData) {
        statuses.add(position, statusViewData);
        notifyItemInserted(position);
    }

    public void clearItems() {
        int oldSize = statuses.size();
        statuses.clear();
        detailedStatusPosition = RecyclerView.NO_POSITION;
        notifyItemRangeRemoved(0, oldSize);
    }

    public void addAll(int position, List<StatusViewData.Concrete> statuses) {
        this.statuses.addAll(position, statuses);
        notifyItemRangeInserted(position, statuses.size());
    }

    public void addAll(List<StatusViewData.Concrete> statuses) {
        int end = statuses.size();
        this.statuses.addAll(statuses);
        notifyItemRangeInserted(end, statuses.size());
    }

    public void removeItem(int position) {
        statuses.remove(position);
        notifyItemRemoved(position);
    }

    public void clear() {
        statuses.clear();
        detailedStatusPosition = RecyclerView.NO_POSITION;
        notifyDataSetChanged();
    }

    public void setItem(int position, StatusViewData.Concrete status, boolean notifyAdapter) {
        statuses.set(position, status);
        if (notifyAdapter) {
            notifyItemChanged(position);
        }
    }

    @Nullable
    public StatusViewData.Concrete getItem(int position) {
        if (position >= 0 && position < statuses.size()) {
            return statuses.get(position);
        } else {
            return null;
        }
    }

    public void setDetailedStatusPosition(int position) {
        if (position != detailedStatusPosition
                && detailedStatusPosition != RecyclerView.NO_POSITION) {
            int prior = detailedStatusPosition;
            detailedStatusPosition = position;
            notifyItemChanged(prior);
        } else {
            detailedStatusPosition = position;
        }
    }

    public int getDetailedStatusPosition() {
        return detailedStatusPosition;
    }
}
