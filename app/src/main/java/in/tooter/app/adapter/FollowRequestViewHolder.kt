/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package `in`.tooter.app.adapter

import android.view.View
import androidx.core.text.BidiFormatter
import androidx.preference.PreferenceManager
import androidx.recyclerview.widget.RecyclerView
import `in`.tooter.app.R
import `in`.tooter.app.entity.Account
import `in`.tooter.app.interfaces.AccountActionListener
import `in`.tooter.app.util.*
import kotlinx.android.synthetic.main.item_follow_request_notification.view.*

internal class FollowRequestViewHolder(itemView: View, private val showHeader: Boolean) : RecyclerView.ViewHolder(itemView) {
    private var id: String? = null
    private val animateAvatar: Boolean = PreferenceManager.getDefaultSharedPreferences(itemView.context)
            .getBoolean("animateGifAvatars", false)

    fun setupWithAccount(account: Account, formatter: BidiFormatter?) {
        id = account.id
        val wrappedName = formatter?.unicodeWrap(account.name) ?: account.name
        val emojifiedName: CharSequence = wrappedName.emojify(account.emojis, itemView)
        itemView.displayNameTextView.text = emojifiedName
        if (showHeader) {
            itemView.notificationTextView?.text = itemView.context.getString(R.string.notification_follow_request_format, emojifiedName)
        }
        itemView.notificationTextView?.visible(showHeader)
        val format = itemView.context.getString(R.string.status_username_format)
        val formattedUsername = String.format(format, account.username)
        itemView.usernameTextView.text = formattedUsername
        val avatarRadius = itemView.avatar.context.resources.getDimensionPixelSize(R.dimen.avatar_radius_48dp)
        loadAvatar(account.avatar, itemView.avatar, avatarRadius, animateAvatar)
    }

    fun setupActionListener(listener: AccountActionListener) {
        itemView.acceptButton.setOnClickListener {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onRespondToFollowRequest(true, id, position)
            }
        }
        itemView.rejectButton.setOnClickListener {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onRespondToFollowRequest(false, id, position)
            }
        }
        itemView.setOnClickListener { listener.onViewAccount(id) }
    }
}
