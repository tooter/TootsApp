/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package `in`.tooter.app.adapter

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import `in`.tooter.app.R
import `in`.tooter.app.interfaces.LinkListener

class HashtagViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    private val hashtag: TextView = itemView.findViewById(R.id.hashtag)

    fun setup(tag: String, listener: LinkListener) {
        hashtag.text = String.format("#%s", tag)
        hashtag.setOnClickListener { listener.onViewTag(tag) }
    }
}