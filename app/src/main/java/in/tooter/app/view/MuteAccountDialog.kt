/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

@file:JvmName("MuteAccountDialog")

package `in`.tooter.app.view

import android.app.Activity
import android.widget.CheckBox
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import `in`.tooter.app.R

fun showMuteAccountDialog(
    activity: Activity,
    accountUsername: String,
    onOk: (notifications: Boolean) -> Unit
) {
    val view = activity.layoutInflater.inflate(R.layout.dialog_mute_account, null)
    (view.findViewById(R.id.warning) as TextView).text =
        activity.getString(R.string.dialog_mute_warning, accountUsername)
    val checkbox: CheckBox = view.findViewById(R.id.checkbox)
    checkbox.setChecked(true)

    AlertDialog.Builder(activity)
            .setView(view)
            .setPositiveButton(android.R.string.ok) { _, _ -> onOk(checkbox.isChecked) }
            .setNegativeButton(android.R.string.cancel, null)
            .show()
}