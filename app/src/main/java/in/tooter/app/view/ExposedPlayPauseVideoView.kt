/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package `in`.tooter.app.view

import android.content.Context
import android.util.AttributeSet
import android.widget.VideoView

class ExposedPlayPauseVideoView @JvmOverloads constructor(
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0)
    : VideoView(context, attrs, defStyleAttr) {

    private var listener: PlayPauseListener? = null

    fun setPlayPauseListener(listener: PlayPauseListener) {
        this.listener = listener
    }

    override fun start() {
        super.start()
        listener?.onPlay()
    }

    override fun pause() {
        super.pause()
        listener?.onPause()
    }

    interface PlayPauseListener {
        fun onPlay()
        fun onPause()
    }
}