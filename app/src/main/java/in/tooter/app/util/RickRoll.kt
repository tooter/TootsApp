/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package `in`.tooter.app.util

import android.content.Context
import android.content.Intent
import android.net.Uri
import `in`.tooter.app.R

fun shouldRickRoll(context: Context, domain: String) =
        context.resources.getStringArray(R.array.rick_roll_domains).any { candidate ->
            domain.equals(candidate, true) || domain.endsWith(".$candidate", true)
        }

fun rickRoll(context: Context) {
    val uri = Uri.parse(context.getString(R.string.rick_roll_url))
    val intent = Intent(Intent.ACTION_VIEW, uri).apply {
        addCategory(Intent.CATEGORY_BROWSABLE)
        addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
    }
    context.startActivity(intent)
}
