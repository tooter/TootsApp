/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package in.tooter.app.util;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextPaint;
import android.text.style.URLSpan;
import android.view.View;

public class CustomURLSpan extends URLSpan {
    public CustomURLSpan(String url) {
        super(url);
    }

    private CustomURLSpan(Parcel src) {
        super(src);
    }

    public static final Parcelable.Creator<CustomURLSpan> CREATOR = new Parcelable.Creator<CustomURLSpan>() {

        @Override
        public CustomURLSpan createFromParcel(Parcel source) {
            return new CustomURLSpan(source);
        }

        @Override
        public CustomURLSpan[] newArray(int size) {
            return new CustomURLSpan[size];
        }

    };

    @Override
    public void onClick(View view) {
        LinkHelper.openLink(getURL(), view.getContext());
    }

    @Override public void updateDrawState(TextPaint ds) {
        super.updateDrawState(ds);
        ds.setUnderlineText(false);
    }
}
