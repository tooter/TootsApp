/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

@file:JvmName("ImageLoadingHelper")

package `in`.tooter.app.util

import android.content.Context
import android.graphics.drawable.BitmapDrawable
import android.widget.ImageView
import androidx.annotation.Px
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import `in`.tooter.app.R


private val centerCropTransformation = CenterCrop()

fun loadAvatar(url: String?, imageView: ImageView, @Px radius: Int, animate: Boolean) {

    if (url.isNullOrBlank()) {
        Glide.with(imageView)
                .load(R.drawable.avatar_default)
                .into(imageView)
    } else {
        if (animate) {
            Glide.with(imageView)
                    .load(url)
                    .transform(
                            centerCropTransformation,
                            RoundedCorners(radius)
                    )
                    .placeholder(R.drawable.avatar_default)
                    .into(imageView)

        } else {
            Glide.with(imageView)
                    .asBitmap()
                    .load(url)
                    .transform(
                            centerCropTransformation,
                            RoundedCorners(radius)
                    )
                    .placeholder(R.drawable.avatar_default)
                    .into(imageView)
        }

    }
}

fun decodeBlurHash(context: Context, blurhash: String): BitmapDrawable {
    return BitmapDrawable(context.resources, BlurHashDecoder.decode(blurhash, 32, 32, 1f))
}