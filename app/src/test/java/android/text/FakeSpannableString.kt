/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package android.text

// Used for stubbing Android implementation without slow & buggy Robolectric things
@Suppress("unused")
class SpannableString(private val text: CharSequence) : Spannable {

    override fun setSpan(what: Any?, start: Int, end: Int, flags: Int) {
        throw NotImplementedError()
    }

    override fun <T : Any?> getSpans(start: Int, end: Int, type: Class<T>?): Array<T> {
        throw NotImplementedError()
    }

    override fun removeSpan(what: Any?) {
        throw NotImplementedError()
    }

    override fun toString(): String {
        return "FakeSpannableString[text=$text]"
    }

    override val length: Int
        get() = text.length


    override fun nextSpanTransition(start: Int, limit: Int, type: Class<*>?): Int {
        throw NotImplementedError()
    }

    override fun getSpanEnd(tag: Any?): Int {
        throw NotImplementedError()
    }

    override fun getSpanFlags(tag: Any?): Int {
        throw NotImplementedError()
    }

    override fun get(index: Int): Char {
        throw NotImplementedError()
    }

    override fun subSequence(startIndex: Int, endIndex: Int): CharSequence {
        throw NotImplementedError()
    }

    override fun getSpanStart(tag: Any?): Int {
        throw NotImplementedError()
    }
}