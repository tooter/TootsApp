/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package `in`.tooter.app.util

import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized

@RunWith(Parameterized::class)
class VersionUtilsTest(
        private val versionString: String,
        private val supportsScheduledToots: Boolean
) {

    companion object {
        @JvmStatic
        @Parameterized.Parameters
        fun data() = listOf(
                arrayOf("2.0.0", false),
                arrayOf("2a9a0", false),
                arrayOf("1.0", false),
                arrayOf("error", false),
                arrayOf("", false),
                arrayOf("2.6.9", false),
                arrayOf("2.7.0", true),
                arrayOf("2.00008.0", true),
                arrayOf("2.7.2 (compatible; Pleroma 1.0.0-1168-ge18c7866-pleroma-dot-site)", true),
                arrayOf("3.0.1", true)
        )
    }

    @Test
    fun testVersionUtils() {
        assertEquals(VersionUtils(versionString).supportsScheduledToots(), supportsScheduledToots)
    }

}