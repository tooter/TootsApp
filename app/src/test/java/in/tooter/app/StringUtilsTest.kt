/*
 *   This program is free software: you can redistribute it and/or modify it under the terms of the GNU General
 *   Public License as published by the Free Software Foundation, either version 3 of the License, or (at your
 *   option) any later version.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even
 *   the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *   General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License along with this program. If not, see
 *   <https://www.gnu.org/licenses/>
 */

package `in`.tooter.app

import `in`.tooter.app.util.dec
import `in`.tooter.app.util.inc
import `in`.tooter.app.util.isLessThan
import org.junit.Assert.*
import org.junit.Test

class StringUtilsTest {
    @Test
    fun isLessThan() {
        val lessList = listOf(
                "abc" to "bcd",
                "ab" to "abc",
                "cb" to "abc",
                "1" to "2"
        )
        lessList.forEach { (l, r) -> assertTrue("$l < $r", l.isLessThan(r)) }
        val notLessList = lessList.map { (l, r) -> r to l } + listOf(
                "abc" to "abc"
        )
        notLessList.forEach { (l, r) -> assertFalse("not $l < $r", l.isLessThan(r)) }
    }

    @Test
    fun inc() {
        listOf(
                "122" to "123",
                "12A" to "12B",
                "1" to "2"
        ).forEach { (l, r) -> assertEquals("$l + 1 = $r", r, l.inc()) }
    }

    @Test
    fun dec() {
        listOf(
                "123" to "122",
                "12B" to "12A",
                "120" to "11z",
                "100" to "zz",
                "0" to "",
                "" to "",
                "2" to "1"
        ).forEach { (l, r) -> assertEquals("$l - 1 = $r", r, l.dec()) }
    }
}