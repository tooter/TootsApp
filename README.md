# Tooter

![](/fastlane/metadata/android/en-US/images/icon.png)

Tooter is an Android client for accessing [Tooter](https://tooter.in), a swadeshi microblogging site.

[<img src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png" alt="Get it on Google Play" height="80" />](https://app.tooter.in)

